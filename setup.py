from setuptools import setup, find_packages

setup(
      name='gringotts',
      version='0.2',
      description='Tools for encryption and password management',
      url='',
      author='Idin',
      author_email='d@idin.net',
      license='',
      packages=find_packages(exclude=("jupyter_tests", ".idea", ".git")),
      install_requires=['send2trash', 'pathlib', 'pandas', 'base32hex', 'datetime', 'aenum', 'dill'],
      python_requires='~=3.5',
      zip_safe=False
)