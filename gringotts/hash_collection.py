import hashlib
import base64, base32hex

def make_hash_sha256(obj, base=64):
    hasher = hashlib.sha256()
    hasher.update(repr(make_hashable(obj)).encode())
    if base==64:
        return base64.b64encode(hasher.digest()).decode()
    elif base==32:
        return base32hex.b32encode(hasher.digest()).replace('=', '-')
    else:
        raise ValueError(f'base{base} is unknown!')


def make_hashable(obj):
    if isinstance(obj, (tuple, list)):
        return tuple((make_hashable(e) for e in obj))

    if isinstance(obj, dict):
        return tuple(sorted((k,make_hashable(v)) for k,v in obj.items()))

    if isinstance(obj, (set, frozenset)):
        return tuple(sorted(make_hashable(e) for e in obj))

    return obj

#o = dict(x=1,b=2,c=[3,4,5],d={6,7})
#print(make_hashable(o))
# (('b', 2), ('c', (3, 4, 5)), ('d', (6, 7)), ('x', 1))

#print(make_hash_sha256(o))
# fyt/gK6D24H9Ugexw+g3lbqnKZ0JAcgtNW+rXIDeU2Y=
