import dill as pickle
import send2trash

class Pickler:
	@staticmethod
	def save(path, obj):
		with open(file=path, mode='wb') as output:
			pickle.dump(obj=obj, file=output, protocol=pickle.HIGHEST_PROTOCOL)

	write = save
	put = save
	dump = save
	pickle = save

	@staticmethod
	def load(path):
		with open(file=path, mode='rb') as input:
			obj = pickle.load(file=input)
		return obj

	read = load
	get = load

	@staticmethod
	def delete(path):
		send2trash.send2trash(path)