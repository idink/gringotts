from .hash_collection import make_hash_sha256

from datetime import datetime
from slytherin.time import Timer

class CacheObj:
	def __init__(self, hashed_args=None, unhashed_args=None, func_name=None):
		if hashed_args is None:
			hashed_args={}
		if unhashed_args is None:
			unhashed_args={}

		self._hashed_args = hashed_args
		self._unhashed_args = unhashed_args
		self._func_name = func_name
		self._result = None
		self._timer = None
		self._creation_time = datetime.now()
		self._times_accessed = 0

	@property
	def hashed_args(self):
		return self._hashed_args

	@property
	def unhashed_args(self):
		return self._unhashed_args

	@property
	def args(self):
		_args = self.hashed_args.copy()
		for key, value in self.unhashed_args.items():
			if key not in self.hashed_args:
				_args[key] = value
		return  _args

	@property
	def hash_dict(self):
		dict = self.hashed_args.copy()
		dict['_func_name_'] = self._func_name
		return dict

	@property
	def cache_key(self):
		return make_hash_sha256(self.hash_dict)

	@property
	def result(self):
		self._times_accessed += 1
		return self._result

	@property
	def timer(self):
		"""
		:rtype: Timer
		"""
		return self._timer

	def run(self, func):
		"""
		:type func: callable
		"""
		self._timer = Timer()
		self._result = func(**self.args)

	def elapsed(self):
		return self.timer.get_elapsed()

	def get_complete_dict(self):
		"""
		:rtype: dict
		"""
		result = self.hash_dict
		result['_func_name_'] = self._func_name
		result['_time_'] = self.timer.start_time
		result['_cache_key_'] = self.cache_key
		result['_times_accessed_'] = self._times_accessed
		return result